package flipkartpom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class flipkartPage {

	public WebDriver driver;

	public flipkartPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	@FindBy(xpath = "//button[@class='_2KpZ6l _2doB4z']")
	private WebElement closePopupButton;

	@FindBy(xpath = "//input[@title='Search for products, brands and more']")
	private WebElement searchBox;

	@FindBy(xpath = "//button[@type='submit']")
	private WebElement searchButton;

	@FindBy(xpath = "(//select[@class='_2YxCDZ'])[1]")
	private WebElement minPriceDropdown;

	@FindBy(xpath = "//option[@value='15000']")
	private WebElement minPriceOptionClick;

	@FindBy(xpath = "(//div[@class='_24_Dny'])[1]")
	private WebElement firstBrand;

	@FindBy(xpath = "(//div[@class='_4rR01T'])[1]")
	private WebElement ClickonPhone;

	public WebDriver getDriver() {
		return driver;
	}

	public WebElement getClosePopupButton() {
		return closePopupButton;
	}

	public WebElement getSearchBox() {
		return searchBox;
	}

	public WebElement getSearchButton() {
		return searchButton;
	}

	public WebElement getMinPriceDropdown() {
		return minPriceDropdown;
	}

	public WebElement getMinPriceOption() {
		return minPriceOptionClick;
	}

	public WebElement getFirstBrand() {
		return firstBrand;
	}

	public WebElement getClickonPhone() {
		return ClickonPhone;
	}

	public void closePopup() {
		WebElement button = getClosePopupButton();
		button.click();

	}

	public void searchBox() {
		WebElement searchBox = getSearchBox();
		searchBox.sendKeys("Mobiles");
		WebElement button = getSearchButton();
		button.click();

	}

	public void minPriceDropdown() throws InterruptedException {
		Thread.sleep(8000);
		WebElement dropdown = getMinPriceDropdown();
		dropdown.click();
		Thread.sleep(5000);
		WebElement priceOption = getMinPriceOption();
		priceOption.click();

	}

	public void clickBrand() throws InterruptedException {
		Thread.sleep(5000);
		WebElement firstBrand2 = getFirstBrand();
		firstBrand2.click();

	}

	public void ClickonPhone() throws InterruptedException {
		Thread.sleep(5000);
		WebElement clickonPhone2 = getClickonPhone();
		clickonPhone2.click();
	}
}
