Feature: Search for mobile phone on Flipkart website

  Scenario: User opens the Flipkart website and performs a search for mobile phones
    Given the user opens the Flipkart website
    When the user searches for mobile phones
    And sets the minimum price range 15000
    And selects a brand
    Then the results should be displayed according to the specified criteria
