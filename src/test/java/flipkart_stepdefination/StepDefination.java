package flipkart_stepdefination;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class StepDefination {
	WebDriver driver;

	@Given("the user opens the Flipkart website")
	public void the_user_opens_the_flipkart_website() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.flipkart.com/");
		flipkartpom.flipkartPage flip = new flipkartpom.flipkartPage(driver);
		flip.closePopup();

	}

	@When("the user searches for mobile phones")
	public void the_user_searches_for_mobile_phones() {
		flipkartpom.flipkartPage flip = new flipkartpom.flipkartPage(driver);
		flip.searchBox();
	}

	@When("sets the minimum price range {int}")
	public void sets_the_minimum_price_range(Integer int1) throws InterruptedException {
		flipkartpom.flipkartPage flip = new flipkartpom.flipkartPage(driver);
		flip.minPriceDropdown();

	}

	@When("selects a brand")
	public void selects_a_brand() throws InterruptedException {
		flipkartpom.flipkartPage flip = new flipkartpom.flipkartPage(driver);
		flip.clickBrand();

	}

	@Then("the results should be displayed according to the specified criteria")
	public void the_results_should_be_displayed_according_to_the_specified_criteria() throws InterruptedException {
		flipkartpom.flipkartPage flip = new flipkartpom.flipkartPage(driver);
		flip.ClickonPhone();

	}

}
